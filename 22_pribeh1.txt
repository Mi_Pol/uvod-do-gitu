Zuzka byla vykutálená uličnice.
Nerada se řídila pravidly a často se topila ve svých průšvizích.
Jednou dostala chuť na rakvičku se šlehačkou. 
Sliny se jí sbíhaly do úst, až musela rychle polykat. 
Jako naschvál nebylo po nějaké cukrárně ani vidu, ani slechu. 
Začala se tedy vyptávat lidí na ulici, jestli neví, kde by si mohla zakoupit rakvičku. 
Všichni udiveně kroutili hlavami a posílali ji do cukrárny. 
Nakonec jednu objevila na kraji sídliště. 
Otevřela dveře a vtom se přihodilo něco, co nečekala… 