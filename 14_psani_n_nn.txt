Hubert byl kouzelnický učeň. 
Všichni braničtí obyvatelé ho měli rádi, přestože se učil na hradě u Trojklanného kamene, protože byli ze zásady pohostinní.  
Navíc jim svými kouzly často pomáhal s přemisťováním kamenných strusek a s pěstováním vinné révy. 
A to nikoli z povinnosti! 
Jen občas, když slunné dny vystřídala mračná obloha a úrodě se nedařilo, bylo to jeho klanovým mistrům kladeno za vinu. 
Ale ať už byli, či nebyli těmito okolnostmi vinní, Hubert jistojistě vinen nebyl. 
