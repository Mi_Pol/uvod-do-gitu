Kvečeru se vyčasilo.
Zrána se ale příliš neoteplilo, a kupodivu se letos vevnitř muselo pořád topit.
Přitom dřeva byl nedostatek – vše vidoucí správce panství obcházel lesy přichystaný penalizovat broky každého, kdo sbírá potají dříví.
Dědeček seděl před chalupou a vzpomínal, že zamlada to tu vypadalo jinak.
Šlechta ze zámku nechodila mezi lid tak zřídka jako dnes a zpravidla se zastavili na kus řeči i tady ve mlýně.
Dědečkova tatínka si tu totiž každý vážil, protože mu na čase tolik nezáleželo a byl ho ochoten věnovat každému notný kus.
Byl to bez debat moudrý muž.
Nadto byl velmi zbožným člověkem a svou náruč křesťansky otevíral každému, kdo se ocitl v nouzi. 