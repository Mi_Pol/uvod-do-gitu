Když viděla Jindru balícího dary, usmála se.
Jak bojoval s balicím papírem, připomínal jí nemotorného štěkajícího psa Rafana, hrajícího si s hadrovým míčkem.  
Vše mu padalo z ruky. 
Lepicí páska se mu přilepila na rukáv, nůžky ustřihly papír křivě – hotová pohroma.
Myslí si, že by ho nevyznamenala žádná hodnoticí komise.  
Ale vždyť je Jindra řídicí pracovník! 
I když pro samotné řízení už nedokáže udělat práci samotnou. 
Jindra si svlékl svrchní úbor přes hlavu a vypadal zoufale. 
„Za chvíli to tady asi zapálím. 
Přichystej hasicí přístroj,“ obrátil se na ni.  
Jen se usmála a řekla: „Jdi se radši projít, já to zabalím za tebe.“ 
