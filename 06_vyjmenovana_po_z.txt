Harry měl pocit, že se z té čarodějnické jazykovědy brzičko zblázní. Stejně z něho bude bystrozor, a ne jazykovědec. 
Kdyby se tak každý jazyk dal rozpitvat stejně snadno jako dračí jazýčky, které prodávali v Medovém ráji. Zítra se ukáže.
 Bude muset vzývat všechny kouzelnické svaté, aby se mu ta zkouška vydařila. Dnes už je ale pozdě, navíc se udělala podezřelá zima. 
 Místností prolétl průvan. Sušený zimostráz stojící ve váze na stole se převrátil a zralé malvazinky se skutálely na podlahu. 
 V rohu místnosti cosi dopadlo na zem. Harry zívl, vytáhl hůlku, aby získal čas reagovat na případného nepřítele. 
